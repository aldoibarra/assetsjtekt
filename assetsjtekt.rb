# encoding: utf-8

#####Nomenclatura de comentarios

	##### Comentario descriptivo de funcionalidad
	## Parte del programa no tan necesaria
	# Parte del programa que se podría borrar

#####codigo de GPIO
##### add the default relative library location to the search path
$:.unshift File.join(File.dirname(__FILE__), '..', 'lib')

require 'socket'
require 'alienreader'
require 'alienconfig'
require 'net/http'
require 'net/https'
require 'json'
require 'uri'

#####This builds an antenna sequence string from a number representing a digital input value.
#####Bits (pins) on the input are used to flag active antennas.
def map_input_to_antennas(dig_in)
	antseq = ""
	for bit in 0..@maxantenna do
		antseq << bit.to_s + " " if dig_in[bit] == 1
	end
	return antseq
end

#####Pasará toda la configuración al lector, basado en el archivo config.ini
def reader_config (reader, config, tipo)
	##Configuraciones generales
	#Limpiamos la lista interna
	reader.clear
	#Configuramos el lector
	reader.taglistformat = config.fetch('tagListForma')
	reader.taglistantennacombine = config.fetch('TagListAntennaCombine')
	reader.taglistcustomformat = config.fetch('TagListCustomFormat')
	reader.antennasequence = config.fetch('AntennaSequence')
	##Configuraciones de adquisición de tags
	#acqg2opsmodeext = config.fetch('AcquireG2OpsMode')
	acqg2opsmodeext = "off"
	#reader.sendreceive('AcqG2OpsMode' + '=' + acqg2opsmodeext.to_s) ##Operación para leer TID
	#reader.acqg2tagdata = config.fetch('AcquireG2TagData') ##Operación para leer TID
	reader.rfmodulation = config.fetch('RfModulation')
	reader.acqg2q = config.fetch('AcqG2Q')
	reader.acqg2qmax = config.fetch('AcqG2QMax')
	reader.acqg2mask = config.fetch('AcqG2Mask')
	reader.acqg2session = config.fetch('AcqG2Session')
	##Configuración de notificaciones
	reader.notifyaddress = config.fetch('notifyaddress')
	reader.notifyformat = config.fetch('NotifyFormat')
	##Configuración de qué hacer en automode
	reader.autoaction = config.fetch('AutoAction')
	#Configuración de estado de las salidas
	reader.autowaitoutput = config.fetch('AutoWaitOutput')
	reader.autoworkoutput = config.fetch('AutoWorkOutput')
	reader.autotrueoutput = config.fetch('AutoTrueOutput')
	reader.autofalseoutput = config.fetch('AutoFalseOutput')
	##configuración de notificaciones y su formato
	reader.notifytrigger = config.fetch('NotifyTrigger')
	tagDataformata = config.fetch('TagDataFormatGroupa')
	tagDataformatb = config.fetch('TagDataFormatGroupb')
	reader.sendreceive(tagDataformata.to_s + '=' + tagDataformatb.to_s)
	reader.notifytime = config.fetch('NotifyTime')
	reader.notifyheader = config.fetch('notifyHeader')
	reader.notifyinclude = config.fetch('notifyInclude')
	##Configuración para stream de GPIO
	reader.iotype = config.fetch('IOtype')
	iopersisttimea = config.fetch('IOpersisttimea')
	iopersisttimeb = config.fetch('IOpersisttimeb')
	reader.sendreceive(iopersisttimea.to_s + '=' + iopersisttimeb.to_s)
	reader.iolistformat = config.fetch('IOListFormat')
	reader.iolistcustomformat = config.fetch('IOListCustomFormat')
	reader.iostreamaddress = config.fetch('IOStreamAddress')
	reader.iostreamformat = config.fetch('IOStreamFormat')
	reader.iostreamcustomformat = config.fetch('IOStreamCustomFormat')
	##tiempo de permanencia de tags en lista interna
	reader.persisttime = config.fetch('PersistTime')
	if tipo == "tags" ## Modo que se activa y desactiva la lectura al activa gpio - ESTABLE
		##Configuración para encender Notificación de tags
		reader.autostarttrigger = config.fetch('AutoStartTrigger')
		reader.autostartpause = config.fetch('AutoStartPause')
		reader.autostoptrigger = config.fetch('AutoStopTrigger')
		reader.autostoptimer = "-1"
		reader.autostoppause = config.fetch('AutoStopPause')
		reader.notifymode = "on"
		puts 'Se configuró para Tags'
	elsif tipo == "timer" ## Modo que se activa con gpio y se desactiva a un determinado tiempo- ESTABLE
		##Configuraciones de automatización de lecturas
		reader.autostarttrigger = config.fetch('AutoStartTrigger')
		reader.autostartpause = config.fetch('AutoStartPause')
		reader.autostoptrigger = "0 0"
		reader.autostoptimer = config.fetch('AutoStopTimer')
		reader.autostoppause = config.fetch('AutoStopPause')
		reader.notifymode = "on"
		puts 'Se configuró para Timer'
	end
	

	return "Se configuró el lector"
end

#####Configuración inicio y fin de lectura
def init_reading (reader, config, status)
	reader.automode = status
	reader.iostreammode = status
	if status == 'on'
		return "Inició lectura"
	elsif status == 'off'
		return "Finalizó lectura"
	end
end

#####Configuramos las estaciones basados en lo configurado en la web
def config_estaciones(reader, hashEst)
	ant = 0
	pot = 0
	puts hashEst
	hashEst.each {|puerto|
    ant = puerto[0].to_i
    pot = puerto[1][1].to_i
	reader.rflevel = [ant,pot]
	}
	return "Se configuraron las estaciones"
end

#####Configuramos las estaciones basados en lo configurado en archivo config.ini
def config_estacioneslocal(reader, config)
	ant0 = config.fetch('ant0')
	ant1 = config.fetch('ant1')
	ant2 = config.fetch('ant2')
	ant3 = config.fetch('ant3')
	reader.rflevel = [0,ant0]
	reader.rflevel = [1,ant1]
	reader.rflevel = [2,ant2]
	reader.rflevel = [3,ant3]
	return "Se configuraron las estaciones"
end

#####Cerrar conexión con lector
def close_rconn (reader)
	reader.automode = 'off'
	reader.iostreammode = "off"
	reader.notifymode = 'off'
	reader.close
	return "Se cerró la conexión con el lector"
end

#####Escuchamos el puerto de notificaciones / procesamos información / enviamos información
def listener_tags (tokenRecv, config, reader)
	# grab various parameters out of a configuration file
	server = TCPServer.new('127.0.0.1', 4000)

	puts' Listener Tags Iniciado'
	##### Continually accept new connections
	loop do
		##### Accept each connection in a separate thread
		Thread.start(server.accept) do |session|
			data = ""
			puts '---------------------------------------'

			##### Read until a \0 (end of message) or nothing (socket closed)
			loop do
				char = session.recv(1)      # read one character
				break if char.length == 0   # socket closed
				data << char                # append character
				if char == "\0"             # end of message
					msg = AlienTagList.new
					msg = data.strip
					puts msg
					portIn = split_portin (msg)
					puts portIn
					portOut = split_portout (msg)
					puts portOut
					type = ""
					#Entrada
					if portIn == 1 && portOut == 2
						type = "in"
						tagstoSend = tag_processAssets(msg, config, type)
						puts "Tags a enviar en json:"
						puts tagstoSend
						puts "Entrada..."
						service_request(tokenRecv, tagstoSend, config)
						puts "Se finalizó la entrada"
						puts '---------------------------------------'
					#Salida
					elsif portIn == 2 && portOut == 1
						type = "out"
						tagstoSend = tag_processAssets(msg, config, type)
						puts "Tags a enviar en json:"
						puts tagstoSend
						puts "Saliendo..."
						service_request(tokenRecv, tagstoSend, config)
						puts "Se finalizó la salida"
						puts '---------------------------------------'
					else
						puts "No se completó el flujo de entrada/salida"
					end	
					data = ""
				end
			end
			puts '(reader closed the socket)'
		end
	end
	return "fin de ciclo del listener"
end

def listener_timer (tokenRecv, config, reader)
	# grab various parameters out of a configuration file

	server = TCPServer.new('127.0.0.1', 4000)

	puts' Listener Tags Iniciado'

	##### Continually accept new connections
	loop do
		##### Accept each connection in a separate thread
		Thread.start(server.accept) do |session|
			data = ""
			puts '---------------------------------------'

			##### Read until a \0 (end of message) or nothing (socket closed)
			loop do
				char = session.recv(1)      # read one character
				break if char.length == 0   # socket closed
				data << char                # append character
				if char == "\0"             # end of message
					msg = AlienTagList.new
					msg = data.strip
					puts msg
					portIn = split_portin (msg)
					reader.open
					puts "Se abrió lector"
					portOut = reader.externalinput
					type = ""
					puts "El puerto a la salida: " + portOut
					###Salida a embarque
					if portIn == 1 && (portOut == "2" || portOut == "3")
						type = "out"
						tagstoSend = tag_processOut(msg, config, type)
						puts "Tags a enviar en json:"
						puts tagstoSend
						puts "Entrada..."
						service_request(tokenRecv, tagstoSend, config,type)
						puts "Se finalizó la Salida"
						puts '---------------------------------------'
					###Entrada a producción
					elsif portIn == 2 && (portOut == "1" || portOut == "3")
						type = "in"
						tagstoSend = tag_processIn(msg, config, type)
						puts "Tags a enviar en json:"
						puts tagstoSend
						puts "Saliendo..."
						service_request(tokenRecv, tagstoSend, config,type)
						puts "Se finalizó la salida"
						puts '---------------------------------------'
					else
						puts "No se completó el flujo de entrada/salida"
					end	
					data = ""
				end
			end
			puts '(reader closed the socket)'
		end
	end
	return "fin de ciclo del listener"
end

def split_portin (msg)
	arraymsg = msg.split("#")
	port = arraymsg[10].split(":")
	port2 = port[1].strip
	portIn = port2.to_i
	return portIn
end

def split_portout (msg)
	arraymsg = msg.split("#")
	port = arraymsg[11].split(":")
	port2 = port[1][0, port[1].length - 6]
	portOut = port2.to_i
	return portOut
end

def tag_listmsg (msg)
	arraymsg = msg.split("Data:")
	listMsg = arraymsg[5]
	puts "Lista de tags: " + listMsg
	return listMsg
end

######Método para procesar tags con formato 
#{"idLector":"5",
#"idComisionado":"5",
#"idUbicacion":"8",
#"idEstación":"9",
#"idCliente":"3",
#"epcs":["000100017001900000000001",
#		"000100017001900000000002",
#		"000100017001900000000005"]
#}

###Proceso para la entrada (lee y envía todo)
def tag_processIn (taglistALN, config, type)
	idreader = config.fetch('idreader')
	cliente = config.fetch('cliente')

	taglistSplt = taglistALN.split("Data:")
	taglistSplt.shift
	#####Ciclo para  crear lista de tags
	btags = Array.new
	taglistSplt.each_with_index {|val, index|
		btags.insert(index, val.split(" ")[1])
	}
	#definimos a dónde se va a transferir
	if type == "in"
		scom = config.fetch('comin')
		subi = config.fetch('ubiin')
		sest = config.fetch('estin')
	elsif type == "out"
		scom = config.fetch('comout')
		subi = config.fetch('ubiout')
		sest = config.fetch('estout')		
	end
	#creamos estructura de json
	if sest == "0" && subi == "0"
		jsontosend = {idLector: idreader, idComisionado: scom, idCliente: cliente, epcs: btags}
	elsif sest == "0"	
		jsontosend = {idLector: idreader, idComisionado: scom, idUbicacion: subi, idCliente: cliente, epcs: btags}
	else
		jsontosend = {idLector: idreader, idComisionado: scom, idUbicacion: subi, idEstacion: sest, idCliente: cliente, epcs: btags}
	end
	puts "Se generó el json"
	return jsontosend.to_json
end

###Proceso para la salida (lee y envía solo 1 tag-el master-)
def tag_processOut (taglistALN, config, type)
	idreader = config.fetch('idreader')
	cliente = config.fetch('cliente')
	taglistSplt = taglistALN.split("Data:")
	taglistSplt.shift
	#####Ciclo para  crear lista de tags
	btags = Array.new
	taglistSplt.each_with_index {|val, index|
		btags.insert(index, val.split(" ")[1])
	}
	#definimos a dónde se va a transferir
	if type == "in"
		scom = config.fetch('comin')
		subi = config.fetch('ubiin')
		sest = config.fetch('estin')
	elsif type == "out"
		scom = config.fetch('comout')
		subi = config.fetch('ubiout')
		sest = config.fetch('estout')		
	end
	#creamos estructura de json
	if sest == "0" && subi == "0"
		jsontosend = {idLector: idreader, idComisionado: scom, idCliente: cliente, epcs: btags[0]}
	elsif sest == "0"	
		jsontosend = {idLector: idreader, idComisionado: scom, idUbicacion: subi, idCliente: cliente, epcs: btags[0]}
	else
		jsontosend = {idLector: idreader, idComisionado: scom, idUbicacion: subi, idEstacion: sest, idCliente: cliente, epcs: btags[0]}
	end
	puts "Se generó el json"
	return jsontosend.to_json
end

##Fin método de envío tags formato para incidencias Assets/Jtekt


#####Método para procesar tags sin TID
def tag_processgpio (taglistALN, ubi, est)
	subi = ubi.to_s
	taglistSplt = taglistALN.split("Data:")
	taglistSplt.shift
	#####Ciclo para reemplazar el puerto por el id de la estación
	btags = Array.new
	taglistSplt.each_with_index {|val, index|
		btags.insert(index, {epc: val.split(" ")[1], id_Estacion: est, id_Ubicaciones: subi})
	}
	puts "Se generó el json"
	return btags.to_json
end

#####Método original para procesar los tags con TID
##def tag_processgpio (taglistALN, ubi, est)
##	subi = ubi.to_s
##	taglistSplt = taglistALN.split("Data:")
##	taglistSplt.shift
##	#####Ciclo para reemplazar el puerto por el id de la estación
##	btags = Array.new
##	taglistSplt.each_with_index {|val, index|
##		btags.insert(index, {epc: val.split(" ")[1], tid: val.split(" ")[3], id_Estacion: est, id_Ubicaciones: subi})
##	}
##	#puts btags
##	puts "Se generó el json"
##	return btags.to_json
##end

#####Procesamiento de tags que reemplaza el puerto de lectura por la estación del sistema y agrega el id de la ubicación según la ip del lector en el comisionado
def tag_process (taglistALN, ubi, orignEst)
	subi = ubi.to_s
	taglistSplt = taglistALN.split("Data:")
	taglistSplt.shift
	puts "Tags a leidos:"
	#####Ciclo para reemplazar el puerto por el id de la estación
	btags = Array.new
	taglistSplt.each_with_index {|val, index|
		orignEst.each {|puerto|
	    idEst = puerto[1][0].to_i
	    puertoEst = puerto[0].to_i
	    #####comparamos que el puerto del que viene el tag (lector) es igual al puerto del hash de las estaciones del lector
	    if val.split(" ")[5].to_i == puertoEst
	    	#####formamos el json a enviar #epc(mensaje lector) #tid(mensaje lector) #id estacion (reemplazado por id del hash) #ubicación del json original de ubicaciones asociada a la ip del lector
	    	btags.insert(index, {epc: val.split(" ")[1], tid: val.split(" ")[3], id_Estacion: idEst, id_Ubicaciones: subi})
		end
		#puts "se generó el json"
		}
	}
	#puts btags
	return btags.to_json
end

def service_request (tokenRecv, objjson, config,type)
	if type == "in"
		urimov = URI(config.fetch('movtagIn'))
	elsif type == "out"
		urimov = URI(config.fetch('movtagIn'))
	end

	https2 = Net::HTTP.new(urimov.host, urimov.port)

	@@ssltype = config.fetch('ssl')
	if @@ssltype == "true"
			https2.use_ssl = true
		else
			https2.use_ssl = false
		end
	https2.verify_mode = OpenSSL::SSL::VERIFY_NONE	
	request = Net::HTTP::Post.new(urimov.request_uri)
	request['Authorization'] = tokenRecv
	request.set_content_type("application/json")
	request.body = objjson
	response = https2.request(request)
	rescue StandardError => error
		puts "no hay servidor"
		puts error.to_s
		rebootlector
  		# HoptoadNotifier.notify error
  		false    # non-success response
			else
         case response
  			when Net::HTTPOK
  				#puts "Se enviaron: ", response.body
  				if response.body.to_i == 0
  					@@blector.open
  					puts "Error, no se hizo la transferencia"
  					@@blector.externaloutput = 12
  					sleep (2)
  					@@blector.externaloutput = 2
  				elsif response.body.to_i > 0
  					@@blector.open
  					puts "Pallet correcto, se hizo la transferencia"
  					@@blector.externaloutput = 1
  					sleep (2)
  					@@blector.externaloutput = 2
  				end
    			true   # success response
  			when Net::HTTPClientError,
      			 Net::HTTPInternalServerError
      			 puts response
      			 puts "mala Respuesta servidor"
    		false  # non-success response
  		end
end

def rebootlector
	clector = @@blector
	##### grab parameters out of a configuration file
	config = AlienConfig.new('config.ini')
	##### change "reader_address" in the config.dat file to the IP address of your reader.
	ipaddress = config.fetch('reader_address', 'localhost')
	clector.externaloutput = 5
	sleep (3)
	#puts clector.open
	if clector.open
		puts "entré a rebootear el lector - estaba abierto"
		puts clector.automode
		clector.automode = 'off'
		puts clector.automode
		clector.notifymode = 'off'
		clector.reboot
		puts "se rebooteo el lector - estaba abierto"
	end
end



begin

	##### grab parameters out of a configuration file
	config = AlienConfig.new('config.ini')

	##### change "reader_address" in the config.dat file to the IP address of your reader.
	ipaddress = config.fetch('reader_address', 'localhost')

	##### create our reader 
	r = AlienReader.new

		#aquí van las configuraciones del lector
	if r.open(ipaddress)

		puts '----------------------------------'
		puts "Connected to #{r.readername}"

		#puts r.open

		@@blector = r

		#####obtenemos ip del lector
		iplector = r.ipaddress
		#puts "Ip traída del lector: " + iplector.to_s

		#####Obtenemos el comisionado
		comisionado = config.fetch('comisionado')
		#####Traemos el modo
		modo = config.fetch('modo')
		#####Traemos id de estación de entrada y salida (gpio 1->2 entrada , gpio 2->1 salida)
		@@entrada = config.fetch('identrada')
		@@salida = config.fetch('idsalida')
		puts "Iniciando programa portal Jtekt"
		r.externaloutput = 1
		#puts "Comisionado: " + comisionado.to_s

		#####QUITAR
		puts "Petición de token"
		sleep(2)
		r.externaloutput = 2

		##### Obtenemos token forma original V2
		uri = URI(config.fetch('rutatoken'))
		puts uri
		https = Net::HTTP.new(uri.host, uri.port)
		@@ssltype = config.fetch('ssl')
		if @@ssltype == "true"
				https.use_ssl = true
			else
				https.use_ssl = false
			end
		https.verify_mode = OpenSSL::SSL::VERIFY_NONE	
		req = Net::HTTP::Post.new(uri.request_uri, initheader = {'Content-Type' =>'application/json'})
		req.body = {username: 'admin', password: 'cGFzc3dvcmQ='}.to_json
		res = https.request(req)
		#####cachamos error en la comunicación con el servidor y reiniciamos
		begin
			rescue StandardError => error
				puts "no hay servidor token"
				puts error.to_s
				rebootlector
		  		false    # non-success response
			else
	         case res
	  			when Net::HTTPOK
	  				puts "Buena respuesta Token"
	    			true   # success response
	  			when Net::HTTPClientError,
	      			 Net::HTTPInternalServerError
	      			 puts "mala Respuesta token"
	    		false  # non-success response
	  			end
	  	end
		#puts res.body

		#####   Guardamos el Token  
		values = res.body.to_s.split('"')
		token = values[3].to_s
		puts "El token es:" + token.to_json
		puts ""

		puts "Petición de estaciones"
		puts ""
		sleep(2)

		r.externaloutput = 4

		#####configuramos el lector
		puts "Configurando Lector"
		puts ""

		respuestaConfig = reader_config(r, config, modo)
		puts respuestaConfig

	######programa para manipuular leds de lector

		#ciclo para configurar cada una de las estaciones
		estConfigantenas = config_estacioneslocal(r, config)
		#puts estConfigantenas

	##### spin here forever... (or ctrl-c) #Agregar configuración para lectura al detectar GPIO
		estLectura = init_reading(r, config, "on")
		puts estLectura

		#iniciamos el listener
		puts "Iniciando listener en modo " + modo
		if modo == 'tags'
		listener_tags(token, config, r)
		elsif modo == 'timer'
		listener_timer(token, config, r)
		end

		#####detenemos el modo automode
		estLectura = init_reading(r, config, "off")
		puts estLectura

	##### be nice. Close the connection to the reader.
		#r.close
		puts close_rconn (r)
	end
rescue
	puts $!
end
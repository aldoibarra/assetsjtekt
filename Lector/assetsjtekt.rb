# encoding: utf-8

#####Nomenclatura de comentarios

	##### Comentario descriptivo de funcionalidad
	## Parte del programa no tan necesaria
	# Parte del programa que se podría borrar

#####codigo de GPIO
##### add the default relative library location to the search path
$:.unshift File.join(File.dirname(__FILE__), '..', 'lib')

require 'socket'
require 'alienreader'
require 'alienconfig'
require 'net/http'
require 'net/https'
require 'json'
require 'uri'

#####This builds an antenna sequence string from a number representing a digital input value.
#####Bits (pins) on the input are used to flag active antennas.
def map_input_to_antennas(dig_in)
	antseq = ""
	for bit in 0..@maxantenna do
		antseq << bit.to_s + " " if dig_in[bit] == 1
	end
	return antseq
end

#####Pasará toda la configuración al lector, basado en el archivo config.ini
def reader_config (reader, config, tipo)
	##Configuraciones generales
	#Limpiamos la lista interna
	reader.clear
	#Configuramos el lector
	reader.taglistformat = config.fetch('tagListForma')
	reader.taglistantennacombine = config.fetch('TagListAntennaCombine')
	reader.taglistcustomformat = config.fetch('TagListCustomFormat')
	reader.antennasequence = config.fetch('AntennaSequence')
	##Configuraciones de adquisición de tags
	#acqg2opsmodeext = config.fetch('AcquireG2OpsMode')
	acqg2opsmodeext = "off"
	#reader.sendreceive('AcqG2OpsMode' + '=' + acqg2opsmodeext.to_s) ##Operación para leer TID
	#reader.acqg2tagdata = config.fetch('AcquireG2TagData') ##Operación para leer TID
	reader.acqg2q = config.fetch('AcqG2Q')
	reader.acqg2qmax = config.fetch('AcqG2QMax')
	reader.acqg2mask = config.fetch('AcqG2Mask')

	##COnfiguración de notificaciones
	reader.notifyaddress = config.fetch('notifyaddress')
	reader.notifyformat = config.fetch('NotifyFormat')
	##Configuración de qué hacer en automode
	reader.autoaction = config.fetch('AutoAction')
	#Configuración de estado de las salidas
	reader.autowaitoutput = config.fetch('AutoWaitOutput')
	reader.autoworkoutput = config.fetch('AutoWorkOutput')
	reader.autotrueoutput = config.fetch('AutoTrueOutput')
	reader.autofalseoutput = config.fetch('AutoFalseOutput')
	##configuración de notificaciones y su formato
	reader.notifytrigger = config.fetch('NotifyTrigger')
	tagDataformata = config.fetch('TagDataFormatGroupa')
	tagDataformatb = config.fetch('TagDataFormatGroupb')
	reader.sendreceive(tagDataformata.to_s + '=' + tagDataformatb.to_s)
	reader.notifytime = config.fetch('NotifyTime')
	reader.notifyheader = config.fetch('notifyHeader')
	reader.notifyinclude = config.fetch('notifyInclude')
	##Configuración para stream de GPIO
	reader.iotype = config.fetch('IOtype')
	iopersisttimea = config.fetch('IOpersisttimea')
	iopersisttimeb = config.fetch('IOpersisttimeb')
	reader.sendreceive(iopersisttimea.to_s + '=' + iopersisttimeb.to_s)
	reader.iolistformat = config.fetch('IOListFormat')
	reader.iolistcustomformat = config.fetch('IOListCustomFormat')
	reader.iostreamaddress = config.fetch('IOStreamAddress')
	reader.iostreamformat = config.fetch('IOStreamFormat')
	reader.iostreamcustomformat = config.fetch('IOStreamCustomFormat')
	##tiempo de permanencia de tags en lista interna
	reader.persisttime = config.fetch('PersistTime')
	if tipo == "tags" ## Modo que se activa y desactiva la lectura al activa gpio - ESTABLE
		##Configuración para encender Notificación de tags
		reader.autostarttrigger = config.fetch('AutoStartTrigger')
		reader.autostartpause = config.fetch('AutoStartPause')
		reader.autostoptrigger = config.fetch('AutoStopTrigger')
		reader.autostoptimer = "-1"
		reader.autostoppause = config.fetch('AutoStopPause')
		reader.notifymode = "on"
		puts 'Se configuró para Tags'
	elsif tipo == "gpio" ##Modo híbrido que usa IOstream - NO ESTABLE
		##Configuraciones de automatización de lecturas
		#reader.autostarttrigger = config.fetch('AutoStartTrigger')
		reader.autostarttrigger = config.fetch('AutoStartTrigger')
		reader.autostartpause = config.fetch('AutoStartPause')
		reader.autostoptimer = config.fetch('AutoStopTimer')
		reader.autostoppause = config.fetch('AutoStopPause')
		#Encendemos stream de gpio
		#reader.iostreammode = config.fetch('IOStreamMode')
		puts 'Se configuró para GPIO'
	elsif tipo == "timer" ## Modo que se activa con gpio y se desactiva a un determinado tiempo- ESTABLE
		##Configuraciones de automatización de lecturas
		reader.autostarttrigger = config.fetch('AutoStartTrigger')
		reader.autostartpause = config.fetch('AutoStartPause')
		reader.autostoptrigger = "0 0"
		reader.autostoptimer = config.fetch('AutoStopTimer')
		reader.autostoppause = config.fetch('AutoStopPause')
		reader.notifymode = "on"
		puts 'Se configuró para GPIO'
	end
	

	return "Se configuró el lector"
end

#####Configuración inicio y fin de lectura
def init_reading (reader, config, status)
	reader.automode = status
	reader.iostreammode = status
	if status == 'on'
		return "Inició lectura"
	elsif status == 'off'
		return "Finalizó lectura"
	end
end

#####Configuramos las estaciones basados en lo configurado en la web
def config_estaciones(reader, hashEst)
	ant = 0
	pot = 0
	puts hashEst
	hashEst.each {|puerto|
    ant = puerto[0].to_i
    pot = puerto[1][1].to_i
	reader.rflevel = [ant,pot]
	}
	return "Se configuraron las estaciones"
end

#####Cerrar conexión con lector
def close_rconn (reader)
	reader.automode = 'off'
	reader.iostreammode = "off"
	reader.notifymode = 'off'
	reader.close
	return "Se cerró la conexión con el lector"
end

##### NO ESTABLE - solo funciona si es el único listener trabajando  
def listener_gpio(ubi, config, reader, tokenRecv)
	#server = TCPServer.new(tcp_listener_address, tcp_listener_port)
	server = TCPServer.new('127.0.0.1', 4001)

	puts' Listener GPIO Iniciado'

	idEstIn = @@entrada
	idEstOut = @@salida
	puts idEstIn
	puts idEstOut
	##### Continually accept new connections
	loop do
		##### Accept each connection in a separate thread
		Thread.start(server.accept) do |session|
			data = ""
			puts '---------------------------------------'
			puts 'Datos GPIO:'
			##### Read until a \0 (end of message) or nothing (socket closed)
			loop do
				char = session.recv(1)      # read one character
				break if char.length == 0   # socket closed
				data << char                # append character
				if char == "\0"             # end of message
					#traemos datos de gpio
					il = data.strip
					tl = AlienTagList.new
					if il != "0"
						puts data.strip			
						##si detectamos una u otra entrada esperamos n tiempo para pedir lista
						if il == "1"
							#validamos conectividad con lector
							reader.open
							reader.iostreammode = "off"		
							puts 'Iniciando entrada...'
							sleep(5)
							estadoIn = reader.externalinput
							tl = reader.taglist
							puts "El estado del gpio es: ", estadoIn
							if estadoIn == "3" || estadoIn == "2" || estadoIn == "1"
								puts tl
								tagstoSend = tag_processgpio(tl, ubi, idEstIn)
								#puts "Tags a enviar en json:"
								#puts tagstoSend
								puts "Se hará envío"
								service_request(tokenRecv, tagstoSend, config)
								puts "Finalizó entrada"
								puts '---------------------------------------'
							else
								puts "Falsa activación"
							end
						elsif il == "2"
							reader.open
							reader.iostreammode = "off"	
							puts 'Iniciando salida...'
							sleep(5)
							estadoIn = reader.externalinput
							tl = reader.taglist
							puts "El estado del gpio es: ", estadoIn
							if estadoIn == "3" || estadoIn == "1" || estadoIn == "2"
								puts tl
								tagstoSend = tag_processgpio(tl, ubi, idEstOut)
								#puts "Tags a enviar en json:"
								#puts tagstoSend
								puts "Se hará envío"
								service_request(tokenRecv, tagstoSend, config)
								puts "Finalizó la salida"
								puts '---------------------------------------'
							else
								puts "Falsa activación"
							end
						elsif il == "0" || il == "3"
							puts "caso 0 o 3"
							#reader.open
							#sleep(6)
							#reader.taglist
							#reader.clear
						end
					else	

					end	
					data = ""
				end
			end
			puts '(reader closed the socket)'
			reader.iostreammode = "on"	
		end
	end
	return "fin de ciclo del listener"
end

#####Escuchamos el puerto de notificaciones / procesamos información / enviamos información
def listener_tags (ubi, orignEst, tokenRecv, config, reader)
	# grab various parameters out of a configuration file

	#server = TCPServer.new(tcp_listener_address, tcp_listener_port)
	server = TCPServer.new('127.0.0.1', 4000)

	puts' Listener Tags Iniciado'

	idEstIn = @@entrada
	idEstOut = @@salida
	puts "id estación entrada: " + idEstIn
	puts "id estación salida: " + idEstOut

	##### Continually accept new connections
	loop do
		##### Accept each connection in a separate thread
		Thread.start(server.accept) do |session|
			data = ""
			puts '---------------------------------------'

			##### Read until a \0 (end of message) or nothing (socket closed)
			loop do
				char = session.recv(1)      # read one character
				break if char.length == 0   # socket closed
				data << char                # append character
				if char == "\0"             # end of message
					msg = AlienTagList.new
					msg = data.strip
					puts msg
					portIn = split_portin (msg)
					puts portIn
					portOut = split_portout (msg)
					puts portOut
					if portIn == 1 && portOut == 2
						tagstoSend = tag_processgpio(msg, ubi, idEstIn)
						puts "Tags a enviar en json:"
						puts tagstoSend
						puts "Entrada..."
						service_request(tokenRecv, tagstoSend, config)
						puts "Se finalizó la entrada"
						puts '---------------------------------------'
					elsif portIn == 2 && portOut == 1
						tagstoSend = tag_processgpio(msg, ubi, idEstOut)
						puts "Tags a enviar en json:"
						puts tagstoSend
						puts "Saliendo..."
						service_request(tokenRecv, tagstoSend, config)
						puts "Se finalizó la salida"
						puts '---------------------------------------'
					else
						puts "No se completó el flujo de entrada/salida"
					end	
					data = ""
				end
			end
			puts '(reader closed the socket)'
		end
	end
	return "fin de ciclo del listener"
end

def listener_timer (ubi, orignEst, tokenRecv, config, reader)
	# grab various parameters out of a configuration file

	server = TCPServer.new('127.0.0.1', 4000)

	puts' Listener Tags Iniciado'

	idEstIn = @@entrada
	idEstOut = @@salida
	puts "id estación entrada: " + idEstIn
	puts "id estación salida: " + idEstOut

	##### Continually accept new connections
	loop do
		##### Accept each connection in a separate thread
		Thread.start(server.accept) do |session|
			data = ""
			puts '---------------------------------------'

			##### Read until a \0 (end of message) or nothing (socket closed)
			loop do
				char = session.recv(1)      # read one character
				break if char.length == 0   # socket closed
				data << char                # append character
				if char == "\0"             # end of message
					msg = AlienTagList.new
					msg = data.strip
					puts msg
					portIn = split_portin (msg)
					#portOut = split_portout (msg)
					reader.open
					puts "Se abrió lector"
					portOut = reader.externalinput
					puts "El puerto a la salida: " + portOut
					if portIn == 1 && (portOut == "2" || portOut == "3")
						tagstoSend = tag_processgpio(msg, ubi, idEstIn)
						puts "Tags a enviar en json:"
						puts tagstoSend
						puts "Entrada..."
						service_request(tokenRecv, tagstoSend, config)
						puts "Se finalizó la entrada"
						puts '---------------------------------------'
					elsif portIn == 2 && (portOut == "1" || portOut == "3")
						tagstoSend = tag_processgpio(msg, ubi, idEstOut)
						puts "Tags a enviar en json:"
						puts tagstoSend
						puts "Saliendo..."
						service_request(tokenRecv, tagstoSend, config)
						puts "Se finalizó la salida"
						puts '---------------------------------------'
					else
						puts "No se completó el flujo de entrada/salida"
					end	
					data = ""
				end
			end
			puts '(reader closed the socket)'
		end
	end
	return "fin de ciclo del listener"
end

def split_portin (msg)
	arraymsg = msg.split("#")
	port = arraymsg[10].split(":")
	port2 = port[1].strip
	portIn = port2.to_i
	return portIn
end

def split_portout (msg)
	arraymsg = msg.split("#")
	port = arraymsg[11].split(":")
	port2 = port[1][0, port[1].length - 6]
	portOut = port2.to_i
	return portOut
end

def tag_listmsg (msg)
	arraymsg = msg.split("Data:")
	listMsg = arraymsg[5]
	puts "Lista de tags: " + listMsg
	return listMsg
end

#####Método para procesar tags sin TID
def tag_processgpio (taglistALN, ubi, est)
	subi = ubi.to_s
	taglistSplt = taglistALN.split("Data:")
	taglistSplt.shift
	#####Ciclo para reemplazar el puerto por el id de la estación
	btags = Array.new
	taglistSplt.each_with_index {|val, index|
		btags.insert(index, {epc: val.split(" ")[1], id_Estacion: est, id_Ubicaciones: subi})
	}
	puts "Se generó el json"
	return btags.to_json
end

#####Método original para procesar los tags con TID
##def tag_processgpio (taglistALN, ubi, est)
##	subi = ubi.to_s
##	taglistSplt = taglistALN.split("Data:")
##	taglistSplt.shift
##	#####Ciclo para reemplazar el puerto por el id de la estación
##	btags = Array.new
##	taglistSplt.each_with_index {|val, index|
##		btags.insert(index, {epc: val.split(" ")[1], tid: val.split(" ")[3], id_Estacion: est, id_Ubicaciones: subi})
##	}
##	#puts btags
##	puts "Se generó el json"
##	return btags.to_json
##end

#####Procesamiento de tags que reemplaza el puerto de lectura por la estación del sistema y agrega el id de la ubicación según la ip del lector en el comisionado
def tag_process (taglistALN, ubi, orignEst)
	subi = ubi.to_s
	taglistSplt = taglistALN.split("Data:")
	taglistSplt.shift
	puts "Tags a leidos:"
	#puts taglistSplt
	#####Ciclo para reemplazar el puerto por el id de la estación
	btags = Array.new
	taglistSplt.each_with_index {|val, index|
		orignEst.each {|puerto|
	    idEst = puerto[1][0].to_i
	    puertoEst = puerto[0].to_i
	    #####comparamos que el puerto del que viene el tag (lector) es igual al puerto del hash de las estaciones del lector
	    if val.split(" ")[5].to_i == puertoEst
	    	#####formamos el json a enviar #epc(mensaje lector) #tid(mensaje lector) #id estacion (reemplazado por id del hash) #ubicación del json original de ubicaciones asociada a la ip del lector
	    	btags.insert(index, {epc: val.split(" ")[1], tid: val.split(" ")[3], id_Estacion: idEst, id_Ubicaciones: subi})
		end
		#puts "se generó el json"
		}
	}
	#puts btags
	return btags.to_json
end

def service_request (tokenRecv, objjson, config)
	urimov = URI(config.fetch('movtags'))
	https2 = Net::HTTP.new(urimov.host, urimov.port)
	https2.use_ssl = true
	https2.verify_mode = OpenSSL::SSL::VERIFY_NONE	
	request = Net::HTTP::Post.new(urimov.request_uri)
	request['Authorization'] = tokenRecv
	request.set_content_type("application/json")
	request.body = objjson
	response = https2.request(request)
	rescue StandardError => error
		puts "no hay servidor"
		puts error.to_s
		rebootlector
  		# HoptoadNotifier.notify error
  		false    # non-success response
			else
         case response
  			when Net::HTTPOK
  				puts "Se enviaron: ", response.body
  				if response.body.to_i == 0
  					@@blector.open
  					puts "Error"
  					@@blector.externaloutput = 4
  					sleep (2)
  					@@blector.externaloutput = 2
  				elsif response.body.to_i > 0
  					@@blector.open
  					puts "Todo chido "
  					@@blector.externaloutput = 1
  					sleep (2)
  					@@blector.externaloutput = 2
  				end
    			true   # success response
  			when Net::HTTPClientError,
      			 Net::HTTPInternalServerError
      			 puts response
      			 puts "mala Respuesta servidor"
    		false  # non-success response
  		end
end

def rebootlector
	clector = @@blector
	##### grab parameters out of a configuration file
	config = AlienConfig.new('config.ini')
	##### change "reader_address" in the config.dat file to the IP address of your reader.
	ipaddress = config.fetch('reader_address', 'localhost')
	clector.externaloutput = 5
	sleep (3)
	#puts clector.open
	if clector.open
		puts "entré a rebootear el lector - estaba abierto"
		puts clector.automode
		clector.automode = 'off'
		puts clector.automode
		clector.notifymode = 'off'
		clector.reboot
		puts "se rebooteo el lector - estaba abierto"
	end
end



begin

	##### grab parameters out of a configuration file
	config = AlienConfig.new('config.ini')

	##### change "reader_address" in the config.dat file to the IP address of your reader.
	ipaddress = config.fetch('reader_address', 'localhost')

	##### create our reader 
	r = AlienReader.new

		#aquí van las configuraciones del lector
	if r.open(ipaddress)

		puts '----------------------------------'
		puts "Connected to #{r.readername}"

		#puts r.open

		@@blector = r

		#####obtenemos ip del lector
		iplector = r.ipaddress
		#puts "Ip traída del lector: " + iplector.to_s

		#####Obtenemos el comisionado
		comisionado = config.fetch('comisionado')
		#####Traemos el modo
		modo = config.fetch('modo')
		#####Traemos id de estación de entrada y salida (gpio 1->2 entrada , gpio 2->1 salida)
		@@entrada = config.fetch('identrada')
		@@salida = config.fetch('idsalida')
		puts "Iniciando programa portal Jtekt"
		r.externaloutput = 1
		#puts "Comisionado: " + comisionado.to_s

		#####QUITAR
		puts "Petición de token"
		sleep(2)
		r.externaloutput = 2

		##### Obtenemos token forma original V2
		uri = URI(config.fetch('rutatoken'))
		puts uri
		https = Net::HTTP.new(uri.host, uri.port)
		https.use_ssl = true
		https.verify_mode = OpenSSL::SSL::VERIFY_NONE	
		req = Net::HTTP::Post.new(uri.request_uri, initheader = {'Content-Type' =>'application/json'})
		req.body = {username: 'admin', password: 'cGFzc3dvcmQ='}.to_json
		res = https.request(req)
		#####cachamos error en la comunicación con el servidor y reiniciamos
		begin
			rescue StandardError => error
				puts "no hay servidor token"
				puts error.to_s
				rebootlector
		  		false    # non-success response
			else
	         case res
	  			when Net::HTTPOK
	  				puts "Buena respuesta Token"
	    			true   # success response
	  			when Net::HTTPClientError,
	      			 Net::HTTPInternalServerError
	      			 puts "mala Respuesta token"
	    		false  # non-success response
	  			end
	  	end
		#puts res.body

		#####   Guardamos el Token  
		values = res.body.to_s.split('"')
		token = values[3].to_s
		puts "El token es:" + token.to_json
		puts ""

		puts "Petición de estaciones"
		puts ""
		sleep(2)
		r.externaloutput = 3

		##Sección para solicitar estaciones en BL
		#####  Solicitamos Estaciones
		uricom = URI(config.fetch('comPC'))
		https2 = Net::HTTP.new(uricom.host, uricom.port)
		https2.use_ssl = true
		https2.verify_mode = OpenSSL::SSL::VERIFY_NONE	
		request = Net::HTTP::Post.new(uricom.request_uri)
		request['Authorization'] = token
		request.set_content_type("application/json")
		request.body = [{id: comisionado}].to_json
		response = https2.request(request)
		#puts response.body
		#####cachamos error en la comunicación con el servidor y reiniciamos en caso de falla
		begin
			rescue StandardError => error
				puts "no hay servidor estaciones"
				puts error.to_s
				rebootlector
		  		# HoptoadNotifier.notify error
		  		false    # non-success response
			else
	         case response
	  			when Net::HTTPOK
	  				puts "Buena respuesta estaciones"
	  				#rebootlector
	    			true   # success response
	  			when Net::HTTPClientError,
	      			 Net::HTTPInternalServerError
	      			 puts "mala Respuesta estaciones"
	    		false  # non-success response
	  			end
	  	end

	  	puts "Decodificando datos de estaciones"
	  	puts ""
	  	sleep(3)
		r.externaloutput = 4

		#####pasamos el json a hash para manipularlo
		estacion = JSON.parse(response.body)


		#####aquí se deberá obtener la ip del lector, ya sea preguntando al lector o mediante el config.ini
		##puts "Introduzca la ip del lector actual:"
		##lectoractual = gets.chomp
		##puts
		lectoractual = iplector # esta linea hace que el lector se obtenga del archivo de configuració, para motivos de prueba se pide el dato al usuario

		#####validación de ip en el paquete recibido existe o no 
		#####guardamos el id del lector (ubicación) en una variable
		idLector = 0
		existeLector = 0
		estacion.each do |lect|
			if lect["ubicaciones"]["ip"] == lectoractual
				existeLector =  1
				idLector = lect["ubicaciones"]["id"]
			end
		end

		#####generación del arreglo que contiene el id, el puerto y la potencia de las estaciones que vienen en el paquete
		estcount = 0
		aEst = Hash.new
		hashEst = Hash.new
		hEst = Hash.new

		if existeLector == 1
			estacion.each do |est|
			lector = est["ubicaciones"]["ip"]
				if lector.to_s == lectoractual
					puts "Ip del lector: "
					puts lector
					puts "Sus estaciones son: "
					puts "   id: " + est["id"].to_s
					aEst[[estcount,0]]= est["id"]
					hashEst[est["puerto"]] = [est["id"],est["potencia"]]
					puts "   Puerto: " + est["puerto"].to_s
					aEst[[estcount,1]]= est["puerto"]
					puts "   Potencia: " + est["potencia"].to_s
					aEst[[estcount,2]]= est["potencia"]
					subestacion = est["subEstaciones"]
					subestacion.each do |subest|
						estcount = estcount + 1
						puts "   ID subestación: " + subest["id"].to_s
						hashEst[subest["puerto"]] = [est["id"],subest["potencia"]]
						aEst[[estcount,0]]= subest["id"]
						puts "   Puerto Sub: " + subest["puerto"].to_s
						aEst[[estcount,1]]= subest["puerto"]
						puts "   Potencia Sub: " + subest["potencia"].to_s
						aEst[[estcount,2]]= subest["potencia"]
					end
					estcount = estcount + 1
				elsif lector.to_s != lectoractual
				end
				puts
			end
		end

		#####configuramos el lector
		puts "Configurando Lector"
		puts ""

		respuestaConfig = reader_config(r, config, modo)
		puts respuestaConfig

	######programa para manipuular leds de lector

		#ciclo para configurar cada una de las estaciones
		estConfigantenas = config_estaciones(r, hashEst)
		puts estConfigantenas

	##### spin here forever... (or ctrl-c) #Agregar configuración para lectura al detectar GPIO
		estLectura = init_reading(r, config, "on")
		puts estLectura
		##sleep 10	

		#iniciamos el listener
		puts "Iniciando listener en modo " + modo
		if modo == 'tags'
		listener_tags(idLector, hashEst, token, config, r)
		elsif modo == 'gpio'
		listener_gpio(idLector, config, r, token)
		elsif modo == 'timer'
		listener_timer(idLector, hashEst, token, config, r)
		end

		#####detenemos el modo automode
		estLectura = init_reading(r, config, "off")
		puts estLectura

	##### be nice. Close the connection to the reader.
		#r.close
		puts close_rconn (r)
	end
rescue
	puts $!
end
#!/bin/sh

USAGE="Usage: `basename $0` {start|stop|status}\r\n"

APP_NAME=assetsjtekt
APP_PATH=/apps/assetsjtekt
APP_SCRIPT="run_assetsjtekt.sh"
APP_CMD="/apps/assetsjtekt/run_assetsjtekt.sh"

STATUS_ON=1
STATUS_OFF=0

[ $# != 1 ] && { echo -ne $USAGE ; exit 1 ; }

case $1 in
  start)
    \ps ax | grep -v $$ | grep -v grep | grep -qs "$APP_CMD" && { echo -ne "service '$APP_NAME' RUNNING\r\n" ; exit 0 ; }

    cd $APP_PATH
    $APP_CMD &
    sleep 1
    cd - &>/dev/null
    STATUS_ON=0
    STATUS_OFF=1
    ;;

  stop)
    killall -q -9 $APP_SCRIPT

    pid=$(\ps ax | grep "$APP_PATH/$APP_NAME" | grep -v grep | grep -v $$ | grep -v $0 | awk '{print $1}')
    if [ "x$pid" != "x" ] ; then
      kill $pid

   # see if it died. force it if it didn't
      sleep 1
      \ps -p $pid > /dev/null
      [ "$?" -eq "0" ] && kill -9 $pid
    fi
    ;;

  status)
    ;;

  *)
    echo -ne "ERROR: invalid ACTION.   $USAGE"
    exit 1
    ;;
esac

if \ps ax | grep "$APP_PATH/$APP_NAME" | grep -v grep | grep -v $$ | grep -qsv $0 ; then
  echo -ne "service '$APP_NAME' RUNNING\r\n"
  exit $STATUS_ON
fi

echo -ne "service '$APP_NAME' STOPPED\r\n"
exit $STATUS_OFF

